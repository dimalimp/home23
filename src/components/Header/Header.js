import './Header.scss';
import Button from '../Button/Button.js';

const Header = () => {
    return (
        <header className="head">
            <div className="container">
                <div className="head__inner">
                    <h1 className="head__text">Текст</h1>
                    <Button />
                </div>
            </div>
        </header>
    )
}

export default Header;