import Egg from '../Egg/Egg';
import './Intro.scss';

const Intro = () => {
    const array = [
        { colorEgg: 'gray', withHover: true },
        { colorEgg: 'yellow' },
        { colorEgg: 'black' },
        { colorEgg: 'yellow' },
        { colorEgg: 'yellow' },
        { colorEgg: 'orange' },
        { colorEgg: 'orange' },
        { colorEgg: 'yellow' },
    ];
    return (
        <section className="intro">
            <div className="container">
                <div className="intro__inner">
                    { array.map((item, index) => (
                        <Egg
                            key={ index }
                            colorEgg={ item.colorEgg }
                            withHover={ item.withHover }
                        />
                    )) }

                </div>
            </div>
        </section>
    )
}

export default Intro;