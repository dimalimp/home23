import { Fragment } from 'react';
import './App.scss';
import '../Header/Header.js'
import Header from '../Header/Header.js';
import Intro from '../Intro/Intro.js';
import Footer from '../Footer/Footer.js';

const App = () => {
  return (
    <Fragment>
      <Header />
      <Intro />
      <Footer />
    </Fragment>
  );
}

export default App;
