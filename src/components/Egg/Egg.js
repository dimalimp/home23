import PropsTypes from 'prop-types';
import './Egg.scss';

const Egg = ({ colorEgg, withHover }) => {
    return (<div className={ `egg intro__egg egg_${colorEgg} ${withHover ? "egg_opacity" : ""}` }></div>)
}

Egg.PropsTypes = {
    colorEgg: PropsTypes.string,
}

export default Egg;