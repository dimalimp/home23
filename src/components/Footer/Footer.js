import './Footer.scss';
import Button from '../Button/Button.js';

const Footer = () => {
    return (
        <footer className="foot">
            <div className="container">
                <Button className=" button_narrow" />
            </div>
        </footer>
    )
}

export default Footer;