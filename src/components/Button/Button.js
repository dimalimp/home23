import { Component } from 'react';
import PropsTypes from 'prop-types';
import './Button.scss';

class Button extends Component {
    click = (e) => {
        e.preventDefault();
        console.log(e.target.innerText)
    }
    render() {
        const { className } = this.props
        return (
            <button onClick={ this.click } className={ `button${className || ""}` }>kнопка-1</button>
        )
    }
}

Button.PropsTypes = {
    className: PropsTypes.string,
}

export default Button;